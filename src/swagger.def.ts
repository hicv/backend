import { swaggerDocs as patientDocs } from '@src/api/v1/patients';

const swagger = {
  openapi: '3.0.0',
  info: {
    title: 'HICV Backend API',
    version: '1.0.0',
    description: 'The REST API for HICV',
  },
  servers: [
    {
      url: 'http://localhost:3000',
      description: 'Development server',
    },
  ],
  paths: {
    ...patientDocs,
  },
};
export default swagger;
