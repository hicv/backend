import { NextFunction, Request, Response } from 'express';

export default (fn: any) =>
  (req: Request, res: Response, next: NextFunction): void => {
    Promise.resolve(fn(req, res, next)).catch((err) => {
      const status = err?.status || 500;
      const model = err?.model;
      const message = err?.message;
      res.status(status);
      res.json({ status, model, message });
    });
  };
