import 'module-alias/register';
import express from 'express';
import swaggerUI from 'swagger-ui-express';

import logger, { morgan } from '@src/utils/logger';
import { PORT } from '@src/environment';

import apis from './api';
import swaggerDocument from './swagger.def';

const app = express();
const port = PORT || 3000;

app.use(express.json());
app.use(morgan);

app.get('/api/health-check', (_req, res) => res.send({ message: 'ok' }));

apis.forEach((api) => {
  const supportApis = [api.controllers];
  supportApis.forEach((api) => {
    app.use(`/api/${api.version}`, api.router);
  });
});

app.use('/api/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.listen(port, () => {
  logger.info(`> Ready on localhost:${port} - env ${process.env.NODE_ENV}`);
});
