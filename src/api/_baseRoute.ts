import express, { Router } from 'express';

interface routeParam {
  version: string;
  routes: Router[];
}

export default ({ version, routes }: routeParam) => {
  const router: Router = express.Router();
  router.use(routes);

  return {
    router,
    version,
  };
};
