import baseRoute from '@src/api/_baseRoute';

import patients from './patients';

export default {
  controllers: baseRoute({
    version: 'v1',
    routes: [patients],
  }),
};
