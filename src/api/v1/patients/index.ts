import router from './patients-controller';

import {
  createPatientSwagger,
  getPatientSwagger,
  resource,
} from './patients-schema';

export const swaggerDocs = {
  [`/api/v1/${resource}`]: {
    get: { ...getPatientSwagger },
    post: { ...createPatientSwagger },
  },
};

export default router;
