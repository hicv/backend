import logger, { loggerName } from '@src/utils/logger';

const SERVICE_NAME = 'patients';

const getPatient = async () => {
  try {
    const result = 'getPatient';
    return result;
  } catch (error) {
    logger.error(loggerName(SERVICE_NAME, 'getPatient'), error);
    throw error;
  }
};

export default { getPatient };
