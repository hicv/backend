import joi from 'joi';
import joiToSwagger from 'joi-to-swagger';

export const resource = 'patients';

// Get patients
export const getPatientSwagger = {
  summary: 'Retrieve the list with all of the patients',
  tags: [resource],
  responses: {
    '200': {
      description: 'Array with patients info',
    },
  },
};

// Get patient by id
export const getPatientByIdSwagger = {
  summary: 'Retrieve a patient by ID',
  tags: [resource],
  responses: {
    '200': {
      description: 'Object with patient info',
    },
  },
};

// Create patient
export const createPatientJoi = joi.object().keys({
  firstName: joi.string().required(),
  lastName: joi.string().required(),
});

const CreatePatientRequest = joiToSwagger(createPatientJoi).swagger;

export const createPatientSwagger = {
  summary: 'Create a new patient',
  tags: [resource],
  requestBody: {
    content: {
      'application/json': {
        schema: { ...CreatePatientRequest },
      },
    },
  },
  responses: {
    '200': {
      description: 'Patient created',
    },
    default: {
      description: 'Error message',
    },
  },
};

export default {
  CreatePatientRequest,
};
