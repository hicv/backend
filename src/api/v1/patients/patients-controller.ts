import express, { Request, Response } from 'express';

import apiWrapper from '@src/middlewares/api-wrapper';

import { resource, createPatientJoi } from './patients-schema';
import patientService from './patients-service';

const router = express.Router();

router.get(
  `/${resource}`,
  apiWrapper(async (_req: Request, res: Response) => {
    const result = await patientService.getPatient();
    res.json({ resource, data: result });
  }),
);

router.post(
  `/${resource}`,
  apiWrapper(async (req: Request, res: Response) => {
    await createPatientJoi.validateAsync(req.body);
    const result = await patientService.getPatient();
    res.json({ resource, data: result });
  }),
);

export default router;
