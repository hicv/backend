import logger from './winston';
import morganMiddleware from './morgan';

export const loggerName = (serviceName: string, functionName: string): string =>
  `${serviceName}:${functionName}: `;

export const morgan = morganMiddleware;

export default logger;
