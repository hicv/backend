<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email, project_title, project_description
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/hicv/backend">
    <img src="images/hicv-logo.jpg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">HICV - Backend API</h3>

  <p align="center">
    This is HICV Backend API.
    <br />
    <a href="https://gitlab.com/hicv/backend"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/hicv/backend">View Demo</a>
    ·
    <a href="https://gitlab.com/hicv/backend/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/hicv/backend/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

HICV Backend API

### Built With

- [Express](https://github.com/expressjs/express)
- [Typescript](https://github.com/microsoft/TypeScript)
- [ESLint](https://github.com/eslint/eslint)
- [Prettier](https://github.com/prettier/prettier)

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- Node v14.17.0 (Recommend)
- Yarn
  ```sh
  npm install yarn -g
  ```
- [Docker](https://www.docker.com/) (Optional)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/hicv/backend.git
   ```
2. Install NPM packages
   ```sh
   yarn install
   ```
3. Create .env file at project root directory
   ```
   NODE_ENV=development
   PORT=3000
   ```
4. Install VSCode Extensions (Recommended)
   ```
   rvest.vs-code-prettier-eslint - to make ESLint, Prettier work smoothly
   ```
5. Run development project
   ```sh
   yarn dev
   ```

<!-- USAGE EXAMPLES -->

## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://gitlab.com/hicv/backend/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->

## Contact

HICV Dev team - [Team members](https://gitlab.com/hicv/backend/-/project_members)

Project Link: [https://gitlab.com/hicv/backend](https://gitlab.com/hicv/backend)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
