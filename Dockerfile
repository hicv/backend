FROM node:14.17-alpine

WORKDIR app
COPY . .

RUN yarn install
RUN yarn build

# TODO: multi stage build

EXPOSE 3000
CMD yarn start