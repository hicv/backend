const gulp = require('gulp');
const through = require('through2');
import { compile } from 'json-schema-to-typescript';
import { forEachChild } from 'typescript';
const fs = require('fs');
const endName = 'schema.ts';
const routes = `../src/api/v1/**/*-${endName}`;
function path(str: string): string {
  let base = str;
  if (base.lastIndexOf(endName) != -1)
    base = base.substring(0, base.lastIndexOf(endName));
  return base;
}
const schema = () => gulp.src(routes).pipe(
  through.obj((chunk: any, enc: any, cb: any) => {
    const filename = chunk.path;
    import(filename).then((schema) => {
      // dynamic import
      console.log('Converting', filename);
      for(const [key, value] of Object.entries(schema.default)) {
        compile(value, `I${key}`).then((ts) => {
          fs.writeFileSync(path(filename).concat('d.ts'), ts);
        });
      }
    });
    cb(null, chunk);
  }),
);


// watch service
const { series } = require('gulp');
export default series(schema);
